package com.example.advancedui

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUI()

    }

    private fun initUI(){
        val richPathList = imgButton.findAllRichPaths()

        var a = 0
        while (a <= 7){
            richPathList[a].setOnPathClickListener {
                Toast.makeText(this,"Connect Clicked",Toast.LENGTH_SHORT).show()
                //RichPathAnimator.animate(x[0]).fillColor(titleColor).start()
                //imgButton.findRichPathByName("pathConnect")!!.fillColor = R.color.connectClicked
                imgButton.setVectorDrawable(R.drawable.vector_home_circle_nav_center_pressed)
                initUI()
            }
            a++
        }

        var b = 8
        while (b <= 23){
            richPathList[b].setOnPathClickListener {
                Toast.makeText(this,"Hyper local Clicked",Toast.LENGTH_SHORT).show()
                //imgButton.findRichPathByIndex(8)!!.fillColor = R.color.connectClicked
                imgButton.setVectorDrawable(R.drawable.vector_home_circle_nav_hyperlocal_pressed)
                initUI()
            }
            b++
        }

        var c = 24
        while (c <= 32){
            richPathList[c].setOnPathClickListener {
                Toast.makeText(this,"Posts Clicked",Toast.LENGTH_SHORT).show()
                //imgButton.findRichPathByIndex(24)!!.fillColor = R.color.connectClicked
                imgButton.setVectorDrawable(R.drawable.vector_home_circle_nav_posts_pressed)
                initUI()
            }
            c++
        }

        var d = 33
        while (d <= 43){
            richPathList[d].setOnPathClickListener {
                Toast.makeText(this,"Fitness Clicked",Toast.LENGTH_SHORT).show()
                //imgButton.findRichPathByIndex(33)!!.fillColor = R.color.connectClicked
                imgButton.setVectorDrawable(R.drawable.vector_home_circle_nav_fitness_pressed)
                initUI()
            }
            d++
        }
    }
}
